#!/bin/bash

DEST_PATH="${DEST_PATH:-/srv/data/home/mysql_backup2}"
RETENTION_DAYS="${RETENTION_DAYS:-15}"

# default on SH: 64M
# some row have a big 4,1Mb big '_elementor_inline_svg' string
#   echo 'select * from BSF_3_postmeta LIMIT 73571,1' | mysql -u root bsforg > home/bla && ls -lh home/bla
MAX_ALLOWED_PACKETS="${MAX_ALLOWED_PACKETS:-256M}"

mkdir -p "$DEST_PATH" || {
    echo >&2 "ERROR: can't create ${DEST_PATH} - Aborting."
    exit 1
}

list_databases() {
    echo 'show databases' \
        | mysql -u root --skip-column-names \
        | grep -E -v '^(information_schema|performance_schema|sys)$'
}
clean_old_backups() {
    find "$DEST_PATH" -name '*.sql.gz' -mtime "+${RETENTION_DAYS}" -ls -delete
}

ts="$( date +%Y%m%d-%H%M%S )"
list_databases \
    | while read -r db ; do
    mysqldump -u root --max-allowed-packet="${MAX_ALLOWED_PACKETS}" "$db" \
        | /bin/gzip -9 \
        > "${DEST_PATH}/${db}-${ts}.sql.gz"
done

clean_old_backups
