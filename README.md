# gandi-sh-terraform

A tool to terraform your Gandi Simple Hosting instances.

## Documentation

The full documentation from the [`docs/`](docs/index.md) directory is published at
<https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-terraform>.
