# gandi-sh-terraform

A tool to terraform your Gandi Simple Hosting instances.

## Features

The script does nothing, because there is no script (yet).

### Current

#### Databases backup

The databases backups is performed by the `mysql_backup_databases.sh` script.

Copy the content of `mysql_backup_databases.anacrontab` into your instance crontab.

### Future

* installs useful binaries (`wp-cli`, `broot`, `tree`, `ncdu`, `whatfiles`)
* set bash aliases (cf `/srv/data/etc/bash/bashrc`)
* installs required SSH keys
* generate `rclone(1)` remotes, `ssh_config` etc
* use `web/customadmin/simplehosting.css`
  in order to visually show which instance you're working on

## Random notes

* no `rsync(1)` support, we'll have to use `rclone(1)` over sFTP.
